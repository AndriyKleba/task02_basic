package com.epam.automation;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        User user = new User();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введіть Імя");
        user.setUserFirstName(scanner.nextLine());
        System.out.println("Введіть Прізвище");
        user.setUserLastName(scanner.nextLine());
        System.out.println("\nПривіт: " + user.getUserFirstName() + " " + user.getUserLastName());
        System.out.println("\n" + user.getUserFirstName() + " " + "вам треба задати діапазон чисел від 1 до 100");
        System.out.println("Введіть перше число: ");
        int firstUserNumber = getNumber();
        System.out.println("Введіть друге число: ");
        int secondUserNumber = getNumber();
        printMenu();

        System.out.println("First number = " + firstUserNumber + " " + "Second Number " + secondUserNumber);
        System.out.println("20 чисел Fibonacci");
        int F1 = 1;
        int F2 = 1;
        int n2;
        System.out.print(F1+" "+F2+" ");
        for(int i = 3; i <= 20; i++){
            n2=F1+F2;
            System.out.print(n2+" ");
            F1=F2;
            F2=n2;
        }
        System.out.println();
    }


    private static int getNumber() {
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            return sc.nextInt();
        } else {
            System.out.println("Помилка введіть число");
            return getNumber();
        }
    }

    private static void printMenu() {
        System.out.println("============Menu============");
        System.out.println("    1. Друк парних чисел");
        System.out.println("    2. Друк непарних чисел");
        System.out.println("============================");
    }


}
