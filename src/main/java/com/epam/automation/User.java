package com.epam.automation;

import java.util.Objects;

public class User {
    private String userFirstName;
    private String userLastName;

    public User() {
    }

    public User(String userFirstName, String userLastName) {
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
    }


    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getUserFirstName(), user.getUserFirstName()) &&
                Objects.equals(getUserLastName(), user.getUserLastName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserFirstName(), getUserLastName());
    }

    @Override
    public String toString() {
        return "User{" +
                "userFirstName='" + userFirstName + '\'' +
                ", userLastName='" + userLastName + '\'' +
                '}';
    }

}
